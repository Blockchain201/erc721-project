const Content_List_Address = '0x355228523232cEe091B69649185037EEe8eE724B'
const Content_List_ABI = [
    {
      "inputs": [],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "owner",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "approved",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "Approval",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "owner",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "operator",
          "type": "address"
        },
        {
          "indexed": false,
          "internalType": "bool",
          "name": "approved",
          "type": "bool"
        }
      ],
      "name": "ApprovalForAll",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "_fromTokenId",
          "type": "uint256"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "_toTokenId",
          "type": "uint256"
        }
      ],
      "name": "BatchMetadataUpdate",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "_tokenId",
          "type": "uint256"
        }
      ],
      "name": "MetadataUpdate",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "price",
          "type": "uint256"
        }
      ],
      "name": "TokenListed",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        },
        {
          "indexed": false,
          "internalType": "address",
          "name": "buyer",
          "type": "address"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "price",
          "type": "uint256"
        }
      ],
      "name": "TokenSold",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "Transfer",
      "type": "event"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "approve",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "owner",
          "type": "address"
        }
      ],
      "name": "balanceOf",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "getApproved",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "owner",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "operator",
          "type": "address"
        }
      ],
      "name": "isApprovedForAll",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [],
      "name": "name",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [],
      "name": "owner",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "ownerOf",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "safeTransferFrom",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        },
        {
          "internalType": "bytes",
          "name": "data",
          "type": "bytes"
        }
      ],
      "name": "safeTransferFrom",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "operator",
          "type": "address"
        },
        {
          "internalType": "bool",
          "name": "approved",
          "type": "bool"
        }
      ],
      "name": "setApprovalForAll",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes4",
          "name": "interfaceId",
          "type": "bytes4"
        }
      ],
      "name": "supportsInterface",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [],
      "name": "symbol",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [],
      "name": "tokenIdCounter",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "name": "tokenPrices",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "tokenURI",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "transferFrom",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_to",
          "type": "address"
        },
        {
          "internalType": "string",
          "name": "_uri",
          "type": "string"
        }
      ],
      "name": "mint",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_tokenId",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "_price",
          "type": "uint256"
        }
      ],
      "name": "listForSale",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_tokenId",
          "type": "uint256"
        }
      ],
      "name": "buy",
      "outputs": [],
      "stateMutability": "payable",
      "type": "function",
      "payable": true
    }
  ]
var contract;
window.addEventListener("load", async () => {
    if (typeof window.ethereum !== 'undefined') {
        window.web3 = new Web3(window.ethereum);

        try {
            await window.ethereum.enable();
        } catch (error) {
            console.error("User denied account access");
        }
    } else {
        console.error("Web3 provider not found. Consider installing MetaMask.");
    }

    contract = new window.web3.eth.Contract(Content_List_ABI, Content_List_Address);
    const accounts = await window.ethereum.request({ method: 'eth_accounts' });

    console.log("Contract Address:", Content_List_Address);
    const tokenid = await contract.methods.tokenIdCounter().call();
    console.log("tokenid", tokenid)
    var fetchPromises = [];

    for (var i = 0; i < tokenid; i++) {
        console.log(i)
        const tokenURL = await contract.methods.tokenURI(i).call();
        const owner = await contract.methods.ownerOf(i).call();
        console.log(tokenURL)
        const fetchPromise = fetch(`https://ipfs.io/ipfs/${tokenURL.slice(7)}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);  
                }
                return response.json();
            })
            .then(dataObject => {
                console.log('Data received as an object:', dataObject);
                return `<li class="product-item">
                        <div class="product-card" tabindex="0">
                            <figure class="product-banner">
                                <img src="https://ipfs.io/ipfs/${dataObject.image.slice(7)}" alt="${dataObject.name}">
                            </figure>
                            <div class="product-content">
                                <a href="#" class="h4 product-title">${dataObject.name}</a>
                                <div>
                                    <div>
                                        <div >
                                            <h4 >Description: ${dataObject.description}</h4>
                                            <a href="#" class="author-username">Owner: ${owner}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>`;
            })
            .catch(error => {
                console.error('Fetch error:', error);
            });

        fetchPromises.push(fetchPromise);
    }

    Promise.all(fetchPromises).then(productArray => {
        console.log("Product array:", productArray);
        // document.querySelector(".product-list").innerHTML = productArray.join('');
        const productLists = document.getElementsByClassName("product-list");
        for (const productList of productLists) {
            productList.innerHTML = productArray.join('');
        }
    });

    var fetchPromises1 = [];
    for (var i = 0; i < tokenid; i++) {
        const tokenURL = await contract.methods.tokenURI(i).call();
        const owner = await contract.methods.ownerOf(i).call();
        const price = await contract.methods.tokenPrices(i).call();
        // console.log(tokenURL);
        // Inside the loop
        console.log(`Token ID: ${i}, Price: ${price}, URL: ${tokenURL}`);
        if (price != 0) {
            const fetchPromise = fetch(`https://ipfs.io/ipfs/${tokenURL.slice(7)}`)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`HTTP error! Status: ${response.status}`);
                    }
                    return response.json();
                })
                .then(dataObject => {
                    console.log('Data received as an object rtyrrwqs:', dataObject);
                    return `<li class="card">
                <div class="card" tabindex="0">
                    <figure class="banner">
                        <img src="https://ipfs.io/ipfs/${dataObject.image.slice(7)}" alt="${dataObject.name}">
                        <div class="actions">
                        </div>
                        <button class="place-bid-btn" data-token-id="${i}" onClick="BUYNFT('${i}','${price}')" >BUY</button>
                    </figure>
                    <div class="product-content">
                        <a href="#" class="h4 title">${dataObject.name}</a>
                        <p>price : ${price}</p>
                        <div>
                            <div>
                                <div>
                                    <h4>Description: ${dataObject.description}</h4>
                                    <a href="#" class="author-username">Owner: ${owner}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>`;
                })
                .catch(error => {
                    console.error('Fetch error:', error);
                });

            fetchPromises1.push(fetchPromise);
        }
    }

    Promise.all(fetchPromises1).then(productArray => {
        console.log("Product array:", productArray);
        // document.querySelector(".product-list").innerHTML = productArray.join('');
        const productLists = document.getElementsByClassName("list");
        for (const productList of productLists) {
            productList.innerHTML = productArray.join('');
        }
    });
        window.BUYNFT = async function (NFTID, price) {
        try {
            // Assuming price is in ether (decimal format)
            const priceInWei = web3.utils.toWei(price,'ether');
            // const priceInWei = window.web3.utils.toWei(priceInWei.toString(), 'ether');
    
            // Now you can use priceInWei in the send function
            alert(priceInWei);
            await contract.methods.buy(NFTID).send({ from: accounts[0], value: priceInWei });
        } catch (error) {
            console.error('Error in BUYNFT:', error);
            // Handle the error here, you might want to show a user-friendly message or take other actions
        }
    };
    



});

document.getElementById('addnft').addEventListener('click', async (e) => {
    e.preventDefault()
    const accounts = await window.ethereum.request({ method: 'eth_accounts' });
    // alert('ndhf')
    var address = document.getElementById('accountAddress').value
    var url = document.getElementById('url').value

    //   console.log(address + tokenId + url)
    try {
        var nftdetails = await contract.methods.mint(accounts[0], url).send({ from: accounts[0] })
        console.log(nftdetails)
    } catch (error) {
        console.log("erroras,jhdbakjsdb", error)
    }

})


