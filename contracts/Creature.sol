// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";

contract ArtNFT is ERC721URIStorage {
    mapping(uint256 => uint256) public tokenPrices;
    address public owner;
    uint256 public tokenIdCounter; // Added tokenIdCounter

    event TokenListed(uint256 tokenId, uint256 price);
    event TokenSold(uint256 tokenId, address buyer, uint256 price);

    modifier onlyOwner() {
        require(msg.sender == owner, "Not the contract owner");
        _;
    }

    constructor() ERC721("ArtNFT", "ANFT") {
        owner = msg.sender;
        tokenIdCounter = 0; // Initialize the tokenIdCounter
    }

    function mint(address _to, string calldata _uri) public {
        uint256 tokenId = tokenIdCounter++;
        _mint(_to, tokenId);
        _setTokenURI(tokenId, _uri);
        
    }

    function listForSale(uint256 _tokenId, uint256 _price) public onlyOwner {
        // Set the token price
        tokenPrices[_tokenId] = _price;

        emit TokenListed(_tokenId, _price);
    }

    function buy(uint256 _tokenId) external payable {
        address tokenOwner = ownerOf(_tokenId);

        // Transfer the token to the buyer
        _transfer(tokenOwner, msg.sender, _tokenId);

        // Transfer the funds to the seller
        payable(tokenOwner).transfer(msg.value);

        // Clear the token price
        tokenPrices[_tokenId] = 0;

        emit TokenSold(_tokenId, msg.sender, msg.value);
    }

    
}
